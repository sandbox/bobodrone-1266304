<?php
/**
 * @file
 * ns_calendar.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function ns_calendar_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "panels" && $api == "layouts") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_views_api().
 */
function ns_calendar_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implementation of hook_node_info().
 */
function ns_calendar_node_info() {
  $items = array(
    'ns_calendar' => array(
      'name' => t('Calendar'),
      'base' => 'node_content',
      'description' => t('A calendar can hold event nodes are attached to it. Each event can have a category attached to it.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'ns_calendar_event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => t('Event nodes are attached to a specific calendar and each event can have a category attached to it.'),
      'has_title' => '1',
      'title_label' => t('Headline'),
      'help' => '',
    ),
  );
  return $items;
}
