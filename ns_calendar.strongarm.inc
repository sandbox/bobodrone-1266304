<?php
/**
 * @file
 * ns_calendar.strongarm.inc
 */

/**
 * Implementation of hook_strongarm().
 */
function ns_calendar_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_ns_calendar';
  $strongarm->value = '1';
  $export['language_content_type_ns_calendar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_ns_calendar_event';
  $strongarm->value = '1';
  $export['language_content_type_ns_calendar_event'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_ns_calendar';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_ns_calendar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_ns_calendar_event';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_ns_calendar_event'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_ns_calendar';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_ns_calendar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_ns_calendar_event';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_ns_calendar_event'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_calendar';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_ns_calendar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_calendar_event';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_ns_calendar_event'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_ns_calendar';
  $strongarm->value = 1;
  $export['node_submitted_ns_calendar'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_ns_calendar_event';
  $strongarm->value = 1;
  $export['node_submitted_ns_calendar_event'] = $strongarm;

  return $export;
}
