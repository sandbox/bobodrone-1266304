<?php
/**
 * @file
 * ns_calendar.features.user_role.inc
 */

/**
 * Implementation of hook_user_default_roles().
 */
function ns_calendar_defaultconfig_user_default_roles() {
  $roles = array();

  // Exported role: calendar administrator
  $roles['calendar administrator'] = array(
    'name' => 'calendar administrator',
    'weight' => '2',
    'uuid' => '1b76b281-9a0e-0094-f991-1172ccb946a4',
  );

  // Exported role: calendar contributor
  $roles['calendar contributor'] = array(
    'name' => 'calendar contributor',
    'weight' => '3',
    'uuid' => '43c164fb-a4ae-9924-5d61-7ec56b563d0d',
  );

  return $roles;
}
