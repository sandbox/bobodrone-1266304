<?php
/**
 * @file
 * ns_calendar.features.user_permission.inc
 */

/**
 * Implementation of hook_user_default_permissions().
 */
function ns_calendar_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: create ns_calendar content
  $permissions['create ns_calendar content'] = array(
    'name' => 'create ns_calendar content',
    'roles' => array(
      0 => 'calendar administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: create ns_calendar_event content
  $permissions['create ns_calendar_event content'] = array(
    'name' => 'create ns_calendar_event content',
    'roles' => array(
      0 => 'calendar administrator',
      1 => 'calendar contributor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any ns_calendar content
  $permissions['delete any ns_calendar content'] = array(
    'name' => 'delete any ns_calendar content',
    'roles' => array(
      0 => 'calendar administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any ns_calendar_event content
  $permissions['delete any ns_calendar_event content'] = array(
    'name' => 'delete any ns_calendar_event content',
    'roles' => array(
      0 => 'calendar administrator',
      1 => 'calendar contributor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own ns_calendar content
  $permissions['delete own ns_calendar content'] = array(
    'name' => 'delete own ns_calendar content',
    'roles' => array(
      0 => 'calendar administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own ns_calendar_event content
  $permissions['delete own ns_calendar_event content'] = array(
    'name' => 'delete own ns_calendar_event content',
    'roles' => array(
      0 => 'calendar administrator',
      1 => 'calendar contributor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any ns_calendar content
  $permissions['edit any ns_calendar content'] = array(
    'name' => 'edit any ns_calendar content',
    'roles' => array(
      0 => 'calendar administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any ns_calendar_event content
  $permissions['edit any ns_calendar_event content'] = array(
    'name' => 'edit any ns_calendar_event content',
    'roles' => array(
      0 => 'calendar administrator',
      1 => 'calendar contributor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own ns_calendar content
  $permissions['edit own ns_calendar content'] = array(
    'name' => 'edit own ns_calendar content',
    'roles' => array(
      0 => 'calendar administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own ns_calendar_event content
  $permissions['edit own ns_calendar_event content'] = array(
    'name' => 'edit own ns_calendar_event content',
    'roles' => array(
      0 => 'calendar administrator',
      1 => 'calendar contributor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
